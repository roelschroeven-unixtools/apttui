#!usr/bin/env python3

import re
import textwrap
from pathlib import Path

# From the Python APT Library, see https://apt-team.pages.debian.net/python-apt/library/index.html
import apt.cache
import apt.package

# Textual, see https://textual.textualize.io
import textual.app as tapp
import textual.containers as tcontainers
import textual.message as tmessage
import textual.widgets as twidgets

def package_matches_filter(package: apt.package.Package, filter_regex_pattern: str) -> bool:
    if re.search(filter_regex_pattern, package.name, re.IGNORECASE):
        return True
    if re.search(filter_regex_pattern, package.versions[-1].description, re.IGNORECASE):
        return True
    return False


def reflow_description(description, width=80):
    org_paragraphs = description.splitlines(keepends=True)
    reflowed_paragraphs = [textwrap.fill(par, width=width) for par in org_paragraphs]
    return '\n'.join(reflowed_paragraphs)


class PackageHighlighted(tmessage.Message):
    def __init__(self, sender: tmessage.MessageTarget, package: apt.package.Package) -> None:
        self.package = package
        super().__init__(sender)


def find_apttui_css():
    module_fn = Path(__file__).resolve()
    containing_dir = module_fn.parent
    if containing_dir.suffix == '.pyz':
        # zipped up in a zipapp (see https://docs.python.org/3/library/zipapp.html#python-api)
        # look in the directory where the zipapp is
        containing_dir = containing_dir.parent
    css_fn = containing_dir / 'apttui.css'
    return str(css_fn)


class PackageList(twidgets.Static):

    BINDINGS = [
        ('f', 'focus_filter_input', 'Filter'),
        ('+', 'add_to_selection', 'Add'),
        ('-', 'remove_from_selection', 'Remove'),
        ('q', 'quit', 'Discard selection and quit'),
        ('f10', 'install_packages', 'Install selected packages and quit'),
    ]

    def __init__(self, apt_cache: apt.cache.Cache, install_selection: set[str]):
        super().__init__()
        self.apt_cache = apt_cache
        self.filter_regex: str = ''
        self.install_selection = install_selection
        self._current_row_key: twidgets.DataTable.RowKey = None

    def compose(self) -> tapp.ComposeResult:
        yield twidgets.DataTable(
            id='package_list',
            show_header=True,
            show_cursor=True,
            zebra_stripes=True,
        )

    def _refresh_table(self) -> None:
        table = self.query_one(twidgets.DataTable)
        table.clear()
        if len(self.filter_regex) < 3:
            self._current_row_key = None
            self.post_message_no_wait(PackageHighlighted(self, None))
            return
        packages = (self.apt_cache[key] for key in self.apt_cache.keys())
        filtered_packages = (pkg for pkg in packages if package_matches_filter(pkg, self.filter_regex))
        table.add_rows(
            (
                (
                '*' if pkg.name in self.install_selection else '',
                '*' if pkg.installed else '',
                pkg.name,
                pkg.versions[-1].summary,
                )
            for pkg
            in sorted(filtered_packages, key=lambda pkg: pkg.name)
            )
        )

    def on_mount(self) -> None:
        table = self.query_one(twidgets.DataTable)
        table.cursor_type = 'row'
        table.add_columns('sel?', 'inst?', 'name', 'description')        

    def set_filter(self, filter_regex: str) -> None:
        self.filter_regex = filter_regex
        self._refresh_table()

    async def on_data_table_row_highlighted(self, message: twidgets.DataTable.RowHighlighted) -> None:       
        table = self.query_one(twidgets.DataTable)
        if table.row_count == 0:
            self._current_row_key = None
            return
        self._current_row_key = message.row_key
        row = table.get_row(message.row_key)
        _, _, name, _ = row
        package = self.apt_cache[name]
        await self.post_message(PackageHighlighted(self, package))

    def action_add_to_selection(self) -> None:
        if self._current_row_key:
            table = self.query_one(twidgets.DataTable)
            row = table.get_row(self._current_row_key)
            _, _, name, _ = row
            table.update_cell(self._current_row_key, table.ordered_columns[0].key, '*')
            self.install_selection.add(name)

    def action_remove_from_selection(self) -> None:
        if self._current_row_key:
            table = self.query_one(twidgets.DataTable)
            row = table.get_row(self._current_row_key)
            _, _, name, _ = row
            table.update_cell(self._current_row_key, table.ordered_columns[0].key, '')
            self.install_selection.remove(name)


class AptTui(tapp.App):
    """Simple interface for browsing and installing apt packages"""

    TITLE = 'apt TUI'
    CSS_PATH = find_apttui_css()

    def __init__(self):
        self.apt_cache = apt.cache.Cache()
        self.apt_cache.open()
        self.install_selection: set[str] = set()
        super().__init__()

    def compose(self) -> tapp.ComposeResult:
        yield twidgets.Header()
        yield tcontainers.Container(
            tcontainers.Vertical(
                PackageList(self.apt_cache, self.install_selection),
                tcontainers.Horizontal(
                    twidgets.Static('Filter (regex):', id='filter_label'),
                    twidgets.Input(id='filter_input'),
                    id='filter_bar',
                )
            ),
            tcontainers.Vertical(
                twidgets.Static(id='package_title'),
                twidgets.Static(id='package_description'),
                id='package_details_container',
            ),
        )
        yield twidgets.Footer()

    def on_mount(self) -> None:
        self.query_one('#package_list', twidgets.DataTable).focus()

    def action_focus_filter_input(self) -> None:
        self.query_one('#filter_input', twidgets.Input).focus()

    def on_input_submitted(self, message: twidgets.Input.Changed) -> None:
        self.query_one(PackageList, PackageList).set_filter(message.value)
        self.query_one('#package_list', twidgets.DataTable).focus()

    def on_package_highlighted(self, message: PackageHighlighted) -> None:
        title_widget = self.query_one('#package_title', twidgets.Static)
        description_widget = self.query_one('#package_description', twidgets.Static)

        package = message.package
        if package is None:
            title_widget.update('')
            description_widget.update('')
        else:
            title_widget.update(f'{package.fullname} | {package.versions[-1].version} | {package.versions[-1].summary}')
            formatted_description = reflow_description(
                package.versions[-1].description,
                width=description_widget.parent.size.width,
                )
            description_widget.update(formatted_description)

    def action_install_packages(self) -> None:
        self.exit(sorted(self.install_selection))


def main():
    app = AptTui()
    packages_to_install = app.run()
    if packages_to_install is None:
        return
    print('Going to install packages:')
    if not packages_to_install:
        print('<none>')
    else:
        print(' '.join(packages_to_install))
        import subprocess
        subprocess.run(['sudo', 'apt-get', 'install'] + packages_to_install)


if __name__ == '__main__':
    main()
