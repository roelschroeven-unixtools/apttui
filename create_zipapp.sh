#!/bin/bash

#if [ -d zipapp ]; then
    #rm zipapp -rf
#fi
#mkdir -p zipapp/apttui

pushd zipapp/apttui
ln -sf ../../apttui.py .
popd

pushd zipapp
python3 -m pip install -r ../requirements.txt --target apttui
ln -sf ../apttui.css .
python3 -m zipapp -p "/usr/bin/env python3" -m "apttui:main" -c apttui
popd
